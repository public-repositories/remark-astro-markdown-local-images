# Why

[Astro](astro.build/) is a website-building framework with excellent support for markdown files right out-of-the-box. However, they recently dropped support for local sourcing of images in markdown files and all images in `.md` files are now sourced from the **/src** folder. This change makes it almost impossible to separate content (say, you store your blog posts in a separate version controlled repository/directory) and the code of the website. 

While this is certainly something that profits bigger, more elaborate websites, it renders astro rather unwieldy for users who want to maintain code-content separation. This [remark](https://remark.js.org/) plugin solves this issue by manipulating local image paths in the [mdast](https://github.com/syntax-tree/mdast) tree. It's not an official solution, and only works reliably for local links to images in the same directory as the `.md` file (e.g., `![image](./image.png)`). 

# Usage

To use this plugin, import the `remarkAstroLocalImages` function, and then include it in the list of remark plugins in your Astro config. Voilà!

Here is a minimal Astro config (typically, the file `astro.config.mjs`) which should work for most users: 
```js 
import { remarkAstroLocalImages } from 'remark-astro-markdown-local-images';

export default defineConfig({

  site: "https://www.mywebsite.xyz",

  markdown: {
    remarkPlugins: [
		remarkAstroLocalImages()
	]
  }

});
```

# Contributing

I think it is a very simple plugin and there's not much else to do here. However, if you find an error, or you have ideas to make the plugin more robust, feel free to send in a merge request. 

